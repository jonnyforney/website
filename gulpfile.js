var gulp = require('gulp');
var sass = require('gulp-sass');
var mini = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('styles', function() {
    gulp.src('assets/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(mini())
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(gulp.dest('./assets/css/'))
});

//Watch task
gulp.task('watch', function() {
    gulp.watch('assets/scss/**/*.scss',['styles']);
});

gulp.task('default', function() {

});
